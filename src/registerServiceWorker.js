import { Workbox } from 'workbox-window';
import { CacheFirst } from 'workbox-strategies';
import { registerRoute } from 'workbox-routing';

// eslint-disable-next-line import/no-mutable-exports
let wb;

if ('serviceWorker' in navigator) {
  wb = new Workbox(`${process.env.BASE_URL}service-worker.js`);

  registerRoute(
    ({ url }) => url.pathname.endsWith('translate'),
    new CacheFirst(),
  );
} else {
  wb = null;
}

export default wb;
