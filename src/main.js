import Buefy from 'buefy';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faExchangeAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import wb from '@/registerServiceWorker.js';
import VueOffline from 'vue-offline';

import Vue from 'vue';
import App from './App.vue';
import router from './router';

import 'buefy/dist/buefy.min.css';

Vue.prototype.$workbox = wb;

Vue.use(Buefy);
Vue.use(VueOffline);

Vue.component('font-awesome-icon', FontAwesomeIcon);

library.add(faExchangeAlt);
library.add(faGitlab);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
